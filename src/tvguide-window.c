/**
 * TV Guide
 * Copyright (c) 2017-2018 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <libintl.h>
#include <gtk/gtk.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>
#include <libxml/xmlreader.h>

#define HANDY_USE_UNSTABLE_API
#include <libhandy-0.0/handy.h>

#include "tvguide-config.h"
#include "tvguide-file.h"
#include "tvguide-xml.h"
#include "tvguide-utils.h"
#include "tvguide-assistant.h"
#include "tvguide-window.h"

#define _(text) gettext (text)

typedef struct
{
  GDateTime *start;
  GDateTime *stop;
  gchar *id;
  gchar *channel;
  gchar *title;
  gchar *subtitle;
  gchar *date;
  gchar *episode;
  gchar *star_rating;
  gchar *link;
  gchar *description;
  gchar *image_url;
  gchar **categories;
  GdkPixbuf *pixbuf;
  GtkWidget *image;
} tv_item;

GHashTable *channels = NULL;
GHashTable *mapping = NULL;
GHashTable *icon_mapping = NULL;

struct _TvGuideWindow
{
  GtkApplicationWindow parent_instance;

  HdyLeaflet *header_box;
  GtkWidget *header_bar;
  GtkWidget *refresh_button;
  GtkWidget *menu_button;
  GtkWidget *sub_header_bar;
  GtkWidget *back_button;
  GtkWidget *next_button;

  GtkWidget *content_stack;
  HdyLeaflet *content_box;
  GtkWidget *assistant;
  GtkWidget *channels_listbox;

  GtkWidget *details_viewport;
  GtkWidget *details_box;

  GtkWidget *search_button;
  GtkWidget *search_bar;
  GtkWidget *search_entry;
  HdyHeaderGroup *header_group;

  guchar type;

  GKeyFile *config;
  gchar **groups;
  gsize config_len;
  gsize index;
};

G_DEFINE_TYPE (TvGuideWindow, tvguide_window, GTK_TYPE_APPLICATION_WINDOW)

tv_item *
parse_item (xmlnode *node)
{
  tv_item *ret;
  xmlnode *tmp;
  const gchar *start;
  const gchar *stop;
  gint year;
  gint month;
  gint day;
  gint hour;
  gint minute;
  gint second;
  gint offset;

  ret = g_malloc0 (sizeof (tv_item));

  start = xmlnode_get_attrib (node, "start");
  sscanf (start, "%4d%2d%2d%2d%2d%2d +%4d", &year, &month, &day, &hour, &minute, &second, &offset);
  ret->start = g_date_time_new_local (year, month, day, hour, minute, second);
  stop = xmlnode_get_attrib (node, "stop");
  sscanf (stop, "%4d%2d%2d%2d%2d%2d +%4d", &year, &month, &day, &hour, &minute, &second, &offset);
  ret->stop = g_date_time_new_local (year, month, day, hour, minute, second);
  ret->id = g_strdup (xmlnode_get_attrib (node, "channel"));
  ret->channel = g_strdup (g_hash_table_lookup (mapping, ret->id));

  for (tmp = node->child; tmp != NULL; tmp = tmp->next) {
    if (tmp->name == NULL) {
      continue;
    }

    if (!strcmp (tmp->name, "title")) {
      ret->title = xmlnode_get_data (tmp);
    } else if (!strcmp (tmp->name, "sub-title")) {
      ret->subtitle = xmlnode_get_data (tmp);
    } else if (!strcmp (tmp->name, "url")) {
      ret->link = xmlnode_get_data (tmp);
    } else if (!strcmp (tmp->name, "desc")) {
      ret->description = xmlnode_get_data (tmp);
    } else if (!strcmp (tmp->name, "category")) {
      ret->categories = tvguide_strv_add (ret->categories, xmlnode_get_data (tmp));
    } else if (!strcmp (tmp->name, "date")) {
      ret->date = xmlnode_get_data (tmp);
    } else if (!strcmp (tmp->name, "episode-num") && !strcmp (xmlnode_get_attrib (tmp, "system"), "onscreen")) {
      ret->episode = xmlnode_get_data (tmp);
    } else if (!strcmp (tmp->name, "star-rating")) {
      xmlnode *child = xmlnode_get_child (tmp, "value");

      if (child != NULL) {
        ret->star_rating = xmlnode_get_data (child);
      }
    }
  }

  return ret;
}

static GSList *
read_xml_data (const gchar *data,
               gsize        len)
{
  xmlnode *node;
  xmlnode *child;
  tv_item *ret;
  GSList *list = NULL;

  node = xmlnode_from_str (data, len);
  g_return_val_if_fail (node != NULL, NULL);

  child = node;
  for (child = xmlnode_get_child (child, "programme"); child != NULL; child = xmlnode_get_next_twin (child)) {
    ret = parse_item (child);
    list = g_slist_prepend (list, ret);
  }

  return list;
}

gchar *
get_category (tv_item *item)
{
  GString *string = g_string_new ("");

  for (int i = 1; item->categories != NULL && i < g_strv_length (item->categories); i++) {
    string = g_string_append (string, item->categories[i]);
    string = g_string_append_c (string, ' ');
  }

  return g_string_free (string, FALSE);
}


gchar *
get_subtitle (tv_item *item)
{
  return g_strdup_printf ("%s%s", item->subtitle != NULL ? "" : "", item->subtitle != NULL ? item->subtitle : "");
}

GdkPixbuf *
load_icon (gchar *group,
           gchar *url)
{
  gchar *filename = g_strdup_printf ("%s.png", group);
  gchar *full_filename = g_strdup_printf ("%s/tvguide/icons/%s", g_get_user_cache_dir (), filename);
  GFile *file = g_file_new_for_path (full_filename);
  gchar *data = NULL;
  gsize file_size;
  GdkPixbuf *pixbuf = NULL;
  gsize size = 32;

  if (!url || strlen (url) == 0) {
    return NULL;
  }

  if (!g_file_query_exists (file, NULL)) {
    g_debug ("File not found (%s), trying to load %s!\n", full_filename, url);

    data = tvguide_read_url (url, &file_size);
    if (data && file_size > 0)
      rm_file_save (full_filename, data, file_size);
  }

  pixbuf = gdk_pixbuf_new_from_file (full_filename, NULL);
  if (pixbuf) {
    return gdk_pixbuf_scale_simple (pixbuf, size, size, GDK_INTERP_BILINEAR);
  }

  return NULL;
}

static gboolean
read_config (gpointer user_data)
{
  TvGuideWindow *self = TVGUIDE_WINDOW (user_data);
  gboolean enabled = g_key_file_get_boolean (self->config, self->groups[self->index], "enabled", NULL);
  gchar *name = g_key_file_get_string (self->config, self->groups[self->index], "name", NULL);
  GDateTime *gdatetime = g_date_time_new_now_local ();
  gint year = g_date_time_get_year (gdatetime);
  gint month = g_date_time_get_month (gdatetime);
  gint day = g_date_time_get_day_of_month (gdatetime);
  gchar *filename = g_strdup_printf ("%s_%4.4d-%2.2d-%2.2d.xml.gz", self->groups[self->index], year, month, day);
  gchar *full_filename = g_strdup_printf ("%s/tvguide/%s", g_get_user_cache_dir (), filename);
  GFile *file = g_file_new_for_path (full_filename);
  gchar *data = NULL;
  gsize file_size;

  g_hash_table_insert (mapping, self->groups[self->index], name);

  gchar *icon_url = g_key_file_get_string (self->config, self->groups[self->index], "icon", NULL);
  GdkPixbuf *icon = load_icon (self->groups[self->index], icon_url);
  if (icon) {
    g_hash_table_insert (icon_mapping, self->groups[self->index], icon);
  } else {
    g_key_file_set_string (self->config, self->groups[self->index], "icon", "");
    tvguide_save_key_file (self->config);
  }

  if (enabled) {
    if (!g_file_query_exists (file, NULL)) {
      gchar *url = g_strdup_printf ("http://xmltv.xmltv.se/%s", filename);
      data = tvguide_read_url (url, &file_size);
      rm_file_save (full_filename, data, file_size);
    } else {
      data = rm_file_load (full_filename, &file_size);
    }

    if (data != NULL) {
      GSList *list = read_xml_data (data, file_size);
      g_hash_table_insert (channels, self->groups[self->index], list);
    }
  }

  self->index++;
  if (self->index < self->config_len)
    return G_SOURCE_CONTINUE;

  tvguide_window_set_time (self, 0);

  return G_SOURCE_REMOVE;
}

static void
remove_old_database (TvGuideWindow *self)
{
  g_autofree gchar *dir_name = g_strdup_printf ("%s/tvguide/", g_get_user_cache_dir ());
  GDateTime *gdatetime = g_date_time_new_now_local ();
  gint year = g_date_time_get_year (gdatetime);
  gint month = g_date_time_get_month (gdatetime);
  gint day = g_date_time_get_day_of_month (gdatetime);
  g_autofree gchar *current_day = g_strdup_printf ("%4.4d-%2.2d-%2.2d", year, month, day);
  GDir *dir = g_dir_open (dir_name, 0, NULL);
  const gchar *name;

  if (!dir)
    return;

  while ((name = g_dir_read_name (dir)) != NULL) {
    g_autofree gchar *filename = g_strdup_printf ("%s/tvguide/%s", g_get_user_cache_dir (), name);

    if (!g_file_test (filename, G_FILE_TEST_IS_DIR) && !strstr (name, current_day))
      g_unlink (filename);
  }
}

gboolean
tvguide_window_load_database (TvGuideWindow *self)
{
  self->config = g_key_file_new ();
  g_autofree gchar *file = g_strdup_printf ("%s/tvguide/tvguide.conf", g_get_user_config_dir ());

  if (!g_key_file_load_from_file (self->config, file, G_KEY_FILE_NONE, NULL)) {
    g_warning ("Could not load config file (%s)\n", file);
    tvguide_window_show_assistant (self);
    return FALSE;
  }

  self->groups = g_key_file_get_groups (self->config, &self->config_len);
  mapping = g_hash_table_new (g_str_hash, g_str_equal);
  icon_mapping = g_hash_table_new (g_str_hash, g_str_equal);
  channels = g_hash_table_new (g_str_hash, g_str_equal);

  remove_old_database (self);
  g_idle_add (read_config, self);

  return TRUE;
}

static void
tvguide_window_clear_channels (GtkWidget *widget,
                               gpointer   data)
{
  gtk_widget_destroy (widget);
}

void
tvguide_window_set_time (TvGuideWindow *self,
                         guchar         type)
{
  GtkWidget *listbox = self->channels_listbox;
  GList *keys;
  GList *key;
  const gchar *entry_text = gtk_entry_get_text (GTK_ENTRY (self->search_entry));

  GDateTime *tnow = g_date_time_new_now_local ();
  GDateTime *t2200 = g_date_time_new_local (g_date_time_get_year (tnow),
                                            g_date_time_get_month (tnow),
                                            g_date_time_get_day_of_month (tnow),
                                            22,
                                            0,
                                            0);
  GDateTime *t2015 = g_date_time_new_local (g_date_time_get_year (tnow),
                                            g_date_time_get_month (tnow),
                                            g_date_time_get_day_of_month (tnow),
                                            20,
                                            15,
                                            0);
  GDateTime *t2359 = g_date_time_new_local (g_date_time_get_year (tnow),
                                            g_date_time_get_month (tnow),
                                            g_date_time_get_day_of_month (tnow),
                                            23,
                                            59,
                                            0);

  self->type = type;

  gtk_stack_set_visible_child_name (GTK_STACK (self->content_stack), "content_box");

  gtk_container_foreach (GTK_CONTAINER (listbox), tvguide_window_clear_channels, NULL);
  keys = g_hash_table_get_keys (channels);
  gtk_list_box_set_header_func (GTK_LIST_BOX (listbox), tvguide_box_header_func, NULL, NULL);

  for (key = keys; key != NULL; key = key->next) {
    GSList *list = g_hash_table_lookup (channels, key->data);

    if (list == NULL) {
      continue;
    }
    GSList *item_list;

    for (item_list = list; item_list != NULL; item_list = item_list->next) {
      tv_item *item = item_list->data;
      GtkWidget *box;
      GtkWidget *label;
      /*gchar *markup; */
      gchar *tmp;
      gboolean add = FALSE;

      switch (type) {
        case 1: {
          if ((g_date_time_compare (item->start, t2015) >= 0) && (g_date_time_compare (item->start, t2200) <= 0)) {
            add = TRUE;
          }
        }
        break;

        default:
        case 0: {
          if ((g_date_time_compare (item->start, tnow) <= 0) && (g_date_time_compare (item->stop, tnow) >= 0)) {
            add = TRUE;
          }
        }
        break;

        case 2: {
          if ((g_date_time_compare (item->start, t2200) >= 0) && (g_date_time_compare (item->start, t2359) <= 0)) {
            add = TRUE;
          }
        }
        break;

        case 4:
          if (entry_text && strlen (entry_text) > 2 && item->title && strcasestr (item->title, entry_text)) {
            add = TRUE;
          }
          break;
      }

      if (add) {
        GdkPixbuf *icon;
        PangoAttrList *attrs;

        box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
        GtkWidget *vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 6);
        gtk_widget_set_margin (box, 0, 6, 0, 6);

        /* Add channel logo */
        item->image = gtk_image_new ();
        icon = g_hash_table_lookup (icon_mapping, item->id);
        if (icon) {
          gtk_image_set_from_pixbuf (GTK_IMAGE (item->image), icon);
        } else {
          gtk_image_set_from_icon_name (GTK_IMAGE (item->image), "video-x-generic-symbolic", GTK_ICON_SIZE_DND);
        }
        gtk_widget_set_size_request (item->image, 48, -1);
        gtk_box_pack_start (GTK_BOX (box), item->image, FALSE, FALSE, 6);
        gtk_box_pack_start (GTK_BOX (box), vbox, TRUE, TRUE, 6);

        /* Add start time + channel name */
        tmp = g_strdup_printf ("%2.2d:%2.2d - %s",
                               g_date_time_get_hour (item->start),
                               g_date_time_get_minute (item->start),
                               item->channel);

        label = gtk_label_new (tmp);
        gtk_widget_set_sensitive (label, FALSE);
        gtk_label_set_xalign (GTK_LABEL (label), 0.0f);
        gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
        gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);

        /* Add title */
        label = gtk_label_new (item->title);
        attrs = pango_attr_list_new ();
        gtk_label_set_xalign (GTK_LABEL (label), 0.0f);
        gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
        pango_attr_list_insert (attrs, pango_attr_weight_new (PANGO_WEIGHT_SEMIBOLD));
        gtk_label_set_attributes (GTK_LABEL (label), attrs);
        /*gtk_widget_set_hexpand (label, TRUE); */
        gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);

        /* Add category */
        label = gtk_label_new (get_category (item));
        gtk_label_set_xalign (GTK_LABEL (label), 0.0f);
        gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
        gtk_widget_set_sensitive (label, FALSE);
        gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);
        /*gtk_grid_attach(GTK_GRID(box), label, 1, 2, 1, 1); */

        GtkWidget *progress = gtk_progress_bar_new ();
        gdouble start = g_date_time_to_unix (item->start);
        gdouble stop = g_date_time_to_unix (item->stop);
        gdouble now = g_date_time_to_unix (g_date_time_new_now_local ());
        /* 5 - 2 = 3 */
        gdouble diff1 = stop - start;
        /* 3 - 2 = 1 */
        gdouble diff2 = now - start;
        gdouble diff = diff2 / diff1;
        gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress), diff);
        if (diff > 0 && diff < 1) {
          gtk_box_pack_end (GTK_BOX (vbox), progress, TRUE, TRUE, 0);
        }

        g_object_set_data (G_OBJECT (box), "item", item);
        gtk_list_box_prepend (GTK_LIST_BOX (listbox), box);
      }
    }
  }
  g_date_time_unref (t2015);
  g_date_time_unref (t2200);
  g_date_time_unref (t2359);

  /*g_signal_connect(listbox, "row-selected") */

  gtk_widget_show_all (listbox);
}

static void
update (TvGuideWindow *self)
{
  GtkWidget *header_child = hdy_leaflet_get_visible_child (self->header_box);
  HdyFold fold = hdy_leaflet_get_fold (self->header_box);

  g_assert (header_child == NULL || GTK_IS_HEADER_BAR (header_child));

  hdy_header_group_set_focus (self->header_group, fold == HDY_FOLD_FOLDED ? GTK_HEADER_BAR (header_child) : NULL);
}

static void
set_information (TvGuideWindow *self,
                 GtkWidget     *child)
{
  /* Remove and free any previous child element */
  if (self->details_box) {
    /* info grid will be unref and destroyed automatically as we have no other ref */
    gtk_container_remove (GTK_CONTAINER (self->details_viewport), self->details_box);
  }

  /* Set new child element */
  self->details_box = child;
  gtk_container_add (GTK_CONTAINER (self->details_viewport), self->details_box);
}

static inline void
set_data (GtkWidget *grid,
          gchar     *name,
          gchar     *entry,
          gboolean   use_markup,
          gint       x,
          gint       y)
{
  GtkWidget *d;
  gchar *markup;

  markup = g_strdup_printf ("<b>%s:</b>", name);

  d = gtk_label_new ("");
  gtk_label_set_markup (GTK_LABEL (d), markup);
  gtk_widget_set_halign (d, GTK_ALIGN_END);
  gtk_widget_set_valign (d, GTK_ALIGN_START);
  gtk_label_set_xalign (GTK_LABEL (d), 1);
  gtk_label_set_yalign (GTK_LABEL (d), 0);
  gtk_grid_attach (GTK_GRID (grid), d, x, y, 1, 1);

  d = gtk_label_new (entry);
  if (use_markup) {
    gtk_label_set_markup (GTK_LABEL (d), entry);
  }

  gtk_label_set_line_wrap (GTK_LABEL (d), TRUE);
  gtk_widget_set_hexpand (d, TRUE);
  gtk_widget_set_halign (d, GTK_ALIGN_START);
  gtk_widget_set_valign (d, GTK_ALIGN_START);
  gtk_label_set_xalign (GTK_LABEL (d), 0);
  gtk_label_set_yalign (GTK_LABEL (d), 0);
  gtk_grid_attach (GTK_GRID (grid), d, x + 1, y, 1, 1);

  g_free (markup);
}

static void
show_details (TvGuideWindow *self,
              tv_item       *item)
{
  GtkWidget *grid;
  GtkWidget *image;
  GtkWidget *label;
  GtkWidget *progress;
  g_autofree gchar *tmp;
  GdkPixbuf *icon;
  gint y = 0;

  grid = gtk_grid_new ();
  gtk_widget_set_hexpand (grid, TRUE);
  gtk_widget_set_vexpand (grid, TRUE);
  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);
  gtk_widget_set_margin (grid, 12, 12, 12, 12);

  icon = g_hash_table_lookup (icon_mapping, item->id);
  if (icon) {
    image = gtk_image_new_from_pixbuf (icon);
    gtk_image_set_pixel_size (GTK_IMAGE (image), GTK_ICON_SIZE_DIALOG);
  } else {
    image = gtk_image_new_from_icon_name ("video-x-generic-symbolic", GTK_ICON_SIZE_DIALOG);
  }
  gtk_grid_attach (GTK_GRID (grid), image, 0, y, 1, 4);

  /* Add start time + channel name */
  tmp = g_strdup_printf ("%2.2d:%2.2d - %s",
                         g_date_time_get_hour (item->start),
                         g_date_time_get_minute (item->start),
                         item->channel);

  label = gtk_label_new (tmp);
  gtk_label_set_xalign (GTK_LABEL (label), 0);
  gtk_widget_set_sensitive (label, FALSE);
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_grid_attach (GTK_GRID (grid), label, 1, y++, 1, 1);

  label = gtk_label_new (item->title != NULL ? item->title : "-");
  gtk_widget_set_hexpand (label, TRUE);
  PangoAttrList *attrs = pango_attr_list_new ();
  pango_attr_list_insert (attrs, pango_attr_weight_new (PANGO_WEIGHT_SEMIBOLD));
  gtk_label_set_attributes (GTK_LABEL (label), attrs);
  gtk_label_set_xalign (GTK_LABEL (label), 0);
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_grid_attach (GTK_GRID (grid), label, 1, y++, 1, 1);

  g_autofree gchar *lstr = NULL;
  if (item->date != NULL) {
    lstr = g_strdup_printf ("%s | %s", get_category (item), item->date);
  } else {
    lstr = g_strdup (get_category (item));
  }
  label = gtk_label_new (lstr);
  gtk_label_set_xalign (GTK_LABEL (label), 0);
  gtk_widget_set_sensitive (label, FALSE);
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_grid_attach (GTK_GRID (grid), label, 1, y++, 1, 1);

  gdouble start = g_date_time_to_unix (item->start);
  gdouble stop = g_date_time_to_unix (item->stop);
  gdouble now = g_date_time_to_unix (g_date_time_new_now_local ());
  /* 5 - 2 = 3 */
  gdouble diff1 = stop - start;
  /* 3 - 2 = 1 */
  gdouble diff2 = now - start;
  gdouble diff = diff2 / diff1;
  progress = gtk_progress_bar_new ();
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress), diff);
  gtk_grid_attach (GTK_GRID (grid), progress, 1, y++, 1, 1);

  /* Separator */
  GtkWidget *separator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
  gtk_grid_attach (GTK_GRID (grid), separator, 0, y++, 2, 1);

  if (item->subtitle != NULL) {
    set_data (grid, _("Sub Title"), item->subtitle, FALSE, 0, y++);
  }

  if (item->description != NULL) {
    set_data (grid, _("Description"), item->description, FALSE, 0, y++);
  }

  if (item->subtitle != NULL || item->description != NULL) {
    separator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
    gtk_grid_attach (GTK_GRID (grid), separator, 0, y++, 2, 1);
  }

  if (item->episode != NULL) {
    set_data (grid, _("Episode"), item->episode, FALSE, 0, y++);
  }

  if (item->star_rating != NULL) {
    set_data (grid, _("Star Rating"), item->star_rating, FALSE, 0, y++);
  }

  if (item->link != NULL) {
    g_autofree gchar *url = g_strdup_printf ("<a href=\"%s\">%s</a>", item->link, item->link);
    set_data (grid, _("Link"), url, TRUE, 0, y++);
  }

  gtk_widget_show_all (grid);
  gtk_widget_set_visible (progress, diff > 0 && diff < 1);
  set_information (self, grid);

  hdy_leaflet_set_visible_child_name (self->content_box, "details");
  hdy_leaflet_set_visible_child_name (self->header_box, "sub_header_bar");
  update (self);
}

static void
tvguide_window_row_activated_cb (GtkListBox    *box,
                                 GtkListBoxRow *row,
                                 TvGuideWindow *self)
{
  tv_item *item = g_object_get_data (gtk_container_get_children (GTK_CONTAINER (row))->data, "item");

  show_details (self, item);
}

static void
tvguide_window_notify_fold_cb (GObject       *sender,
                               GParamSpec    *pspec,
                               TvGuideWindow *self)
{
  update (self);
}

static void
tvguide_window_notify_visible_child_cb (GObject       *sender,
                                        GParamSpec    *pspec,
                                        TvGuideWindow *self)
{
  hdy_leaflet_set_visible_child_name (self->content_box, "details");
}

static void
tvguide_window_notify_header_visible_child_cb (GObject       *sender,
                                               GParamSpec    *pspec,
                                               TvGuideWindow *self)
{
  update (self);
}

static void
tvguide_window_back_button_clicked_cb (GtkWidget     *sender,
                                       TvGuideWindow *self)
{
  hdy_leaflet_set_visible_child_name (self->content_box, "channels");
}

static void
tvguide_window_refresh_clicked_cb (GtkWidget     *sender,
                                   TvGuideWindow *self)
{
  tvguide_window_set_time (self, self->type);
}

void
on_search_entry_search_changed (GtkSearchEntry *entry,
                                gpointer        user_data)
{
  TvGuideWindow *self = TVGUIDE_WINDOW (user_data);

  tvguide_window_set_time (self, 4);
}

static gboolean
on_key_press_event (GtkWidget *widget,
                    GdkEvent  *event,
                    gpointer   user_data)
{
  TvGuideWindow *self = TVGUIDE_WINDOW (widget);
  GdkEventKey *key = (GdkEventKey*)event;
  gint ret;

  ret = gtk_search_bar_handle_event(GTK_SEARCH_BAR(self->search_bar), event);

  if (ret != GDK_EVENT_STOP) {
      if (key->keyval == GDK_KEY_Escape) {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(self->search_button), FALSE);
        tvguide_window_set_time (self, 0);
      }
  } else {
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(self->search_button), TRUE);
  }

  return ret;
}


static void
tvguide_window_constructed (GObject *object)
{
  TvGuideWindow *self = TVGUIDE_WINDOW (object);
  GtkWidget *menu_popover;
  GtkBuilder *builder;

  G_OBJECT_CLASS (tvguide_window_parent_class)->constructed (object);

  hdy_leaflet_set_visible_child_name (self->content_box, "channels");

  /* Set popover menu */
  builder = gtk_builder_new_from_resource ("/org/tabos/tvguide/ui/tvguide-menu-popover.ui");
  menu_popover = GTK_WIDGET (gtk_builder_get_object (builder, "menu-popover"));
  gtk_menu_button_set_popover (GTK_MENU_BUTTON (self->menu_button), menu_popover);
  g_object_unref (builder);
}

static void
tvguide_window_class_init (TvGuideWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = tvguide_window_constructed;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/tabos/tvguide/ui/tvguide-window.ui");

  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, header_box);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, menu_button);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, sub_header_bar);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, back_button);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, next_button);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, content_stack);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, content_box);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, channels_listbox);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, details_viewport);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, details_box);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, search_button);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, search_bar);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, search_entry);
  gtk_widget_class_bind_template_child (widget_class, TvGuideWindow, header_group);

  gtk_widget_class_bind_template_callback_full (widget_class, "notify_fold_cb", G_CALLBACK (tvguide_window_notify_fold_cb));
  gtk_widget_class_bind_template_callback_full (widget_class, "notify_visible_child_cb", G_CALLBACK (tvguide_window_notify_visible_child_cb));
  gtk_widget_class_bind_template_callback_full (widget_class, "notify_header_visible_child_cb", G_CALLBACK(tvguide_window_notify_header_visible_child_cb));
  gtk_widget_class_bind_template_callback_full (widget_class, "back_button_clicked_cb", G_CALLBACK (tvguide_window_back_button_clicked_cb));
  gtk_widget_class_bind_template_callback_full (widget_class, "row_activated_cb", G_CALLBACK (tvguide_window_row_activated_cb));
  gtk_widget_class_bind_template_callback_full (widget_class, "refresh_clicked_cb", G_CALLBACK (tvguide_window_refresh_clicked_cb));
  gtk_widget_class_bind_template_callback_full (widget_class, "on_search_entry_search_changed", G_CALLBACK (on_search_entry_search_changed));
  gtk_widget_class_bind_template_callback_full (widget_class, "on_key_press_event", G_CALLBACK (on_key_press_event));
}

static void
tvguide_window_init (TvGuideWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  g_object_bind_property (self->search_button, "active", self->search_bar, "search-mode-enabled", 0);

  update (self);
}

TvGuideWindow *
tvguide_window_new (GtkApplication *application)
{
  return g_object_new (TVGUIDE_TYPE_WINDOW, "application", application, NULL);
}

void
tvguide_window_show_assistant (TvGuideWindow *self)
{
  TvGuideAssistant *assistant = tvguide_assistant_new (self);

  gtk_widget_show_all (GTK_WIDGET (assistant));
}

void
tvguide_window_show_main (TvGuideWindow *self)
{
  gtk_widget_show (GTK_WIDGET (self));
}

static void
about_response (GtkWidget *widget,
                gpointer   user_data)
{
  gtk_widget_destroy (widget);
}

void
tvguide_window_show_about (TvGuideWindow *self)
{
  GtkWidget *dialog = NULL;
  g_autoptr (GdkPixbuf) pixbuf = NULL;
  g_autoptr (GdkPixbuf) scaled_pixbuf = NULL;
  const gchar *authors[] = {
    "Jan-Michael Brummer <jan.brummer@tabos.org>",
    NULL
  };
  const gchar *documenters[] = {
    "Jan-Michael Brummer <jan.brummer@tabos.org>",
    NULL
  };
  gchar *translators =
    "Jan-Michael Brummer <jan.brummer@tabos.org>";

  /* create about dialog */
  dialog = gtk_about_dialog_new ();

  gtk_about_dialog_set_program_name (GTK_ABOUT_DIALOG (dialog), PACKAGE_NAME);
  gtk_about_dialog_set_version (GTK_ABOUT_DIALOG (dialog), PACKAGE_VERSION);
  gtk_about_dialog_set_copyright (GTK_ABOUT_DIALOG (dialog), "(C) 2018-2019, Jan-Michael Brummer <jan.brummer@tabos.org>");

  gtk_about_dialog_set_license_type (GTK_ABOUT_DIALOG (dialog), GTK_LICENSE_GPL_3_0);
  gtk_about_dialog_set_wrap_license (GTK_ABOUT_DIALOG (dialog), TRUE);
  gtk_about_dialog_set_authors (GTK_ABOUT_DIALOG (dialog), authors);
  gtk_about_dialog_set_documenters (GTK_ABOUT_DIALOG (dialog), documenters);
  gtk_about_dialog_set_translator_credits (GTK_ABOUT_DIALOG (dialog), g_locale_to_utf8 (translators, -1, 0, 0, 0));
  gtk_about_dialog_set_website (GTK_ABOUT_DIALOG (dialog), PACKAGE_WEBSITE);
  pixbuf = gdk_pixbuf_new_from_resource ("/org/tabos/tvguide/org.tabos.tvguide.png", NULL);
  scaled_pixbuf = gdk_pixbuf_scale_simple (pixbuf, 128, 128, GDK_INTERP_BILINEAR);

  gtk_about_dialog_set_logo (GTK_ABOUT_DIALOG (dialog), scaled_pixbuf);
  g_signal_connect (G_OBJECT (dialog), "response", G_CALLBACK (about_response), dialog);

  gtk_widget_show_all (dialog);
}
