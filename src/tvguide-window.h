/**
 * TV Guide
 * Copyright (c) 2017-2018 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TVGUIDE_TYPE_WINDOW (tvguide_window_get_type())

G_DECLARE_FINAL_TYPE (TvGuideWindow, tvguide_window, TVGUIDE, WINDOW, GtkApplicationWindow)

TvGuideWindow *tvguide_window_new (GtkApplication *application);
void tvguide_window_show_assistant (TvGuideWindow *self);
void tvguide_window_show_about (TvGuideWindow *self);
void tvguide_window_set_time (TvGuideWindow *self, guchar type);
void tvguide_window_show_main (TvGuideWindow *self);
gboolean tvguide_window_load_database (TvGuideWindow *self);

G_END_DECLS

