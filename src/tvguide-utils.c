/**
 * TV Guide
 * Copyright (c) 2017-2018 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <gtk/gtk.h>

#include <libsoup/soup.h>

SoupSession *soup_session_sync = NULL;
static GMutex m;

gchar *
tvguide_read_url (gchar *url,
                  gsize *len)
{
  SoupMessage *msg;
  gchar *data;

  g_mutex_lock (&m);
  msg = soup_message_new (SOUP_METHOD_GET, url);
  soup_session_send_message (soup_session_sync, msg);
  if (msg->status_code != 200) {
    g_debug ("Could not read url '%s', status: %s (%d)", url, soup_status_get_phrase (msg->status_code), msg->status_code);
    g_object_unref (msg);

    g_mutex_unlock (&m);
    return NULL;
  }

  *len = msg->response_body->length;
  data = g_malloc (*len);
  memcpy (data, msg->response_body->data, *len);
  g_mutex_unlock (&m);

  return data;
}

void
tvguide_box_header_func (GtkListBoxRow *row,
                         GtkListBoxRow *before,
                         gpointer       user_data)
{
  GtkWidget *current;

  if (!before) {
    gtk_list_box_row_set_header (row, NULL);
    return;
  }

  current = gtk_list_box_row_get_header (row);
  if (!current) {
    current = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
    gtk_widget_show (current);
    gtk_list_box_row_set_header (row, current);
  }
}

gboolean
tvguide_strv_contains(const gchar * const *strv,
                      const gchar         *str)
{
  g_return_val_if_fail (strv != NULL, FALSE);
  g_return_val_if_fail (str != NULL, FALSE);

  for (; *strv != NULL; strv++) {
    g_print ("%s <-> %s\n", str, *strv);
    if (g_str_equal (str, *strv)) {
      return TRUE;
    }
  }

  return FALSE;
}

gchar **
tvguide_strv_add (gchar       **strv,
                  const gchar  *str)
{
  guint len = strv ? g_strv_length (strv) : 0;
  gint i = 0;
  gchar **new_strv;

  new_strv = g_malloc0 (sizeof (gchar *) * (len + 2));

  for (i = 0; i < len; i++) {
    new_strv[i] = g_strdup (strv[i]);
  }

  if (!strv || !g_strv_contains ((const gchar * const *) strv, str)) {
    new_strv[i] = g_strdup (str);
    new_strv[i + 1] = NULL;
  }

  return new_strv;
}

void
tvguide_save_key_file (GKeyFile *file)
{
  g_autofree gchar *filename = g_strdup_printf ("%s/tvguide/tvguide.conf", g_get_user_config_dir ());
  g_mkdir_with_parents (g_path_get_dirname (filename), 0700);

  g_key_file_save_to_file (file, filename, NULL);
}

