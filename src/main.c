/**
 * TV Guide
 * Copyright (c) 2017-2018 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libintl.h>
#include <gtk/gtk.h>
#include <libsoup/soup.h>


#define HANDY_USE_UNSTABLE_API
#include <handy.h>

#include "tvguide-assistant.h"
#include "tvguide-config.h"
#include "tvguide-window.h"

extern SoupSession *soup_session_sync;

static GtkApplication *app = NULL;
static TvGuideWindow *window = NULL;

static void
app_assistant (GSimpleAction *action,
               GVariant      *parameter,
               gpointer       user_data)
{
  tvguide_window_show_assistant (window);
}

static void
app_schedule_now (GSimpleAction *action,
                  GVariant      *parameter,
                  gpointer       user_data)
{
  tvguide_window_set_time (window, 0);
}

static void
app_schedule_prime_time (GSimpleAction *action,
                         GVariant      *parameter,
                         gpointer       user_data)
{
  tvguide_window_set_time (window, 1);
}

static void
app_schedule_late_time (GSimpleAction *action,
                        GVariant      *parameter,
                        gpointer       user_data)
{
  tvguide_window_set_time (window, 2);
}

static void
app_about (GSimpleAction *action,
           GVariant      *parameter,
           gpointer       user_data)
{
  tvguide_window_show_about (window);
}

static GActionEntry app_entries[] =
{
  { "assistant", app_assistant, NULL, NULL, NULL },
  { "schedule_now", app_schedule_now, NULL, NULL, NULL },
  { "schedule_prime_time", app_schedule_prime_time, NULL, NULL, NULL },
  { "schedule_late_time", app_schedule_late_time, NULL, NULL, NULL },
  { "about", app_about, NULL, NULL, NULL },
};

static void
startup (GtkApplication *app)
{
  GtkCssProvider *css_provider = gtk_css_provider_new ();

  gtk_css_provider_load_from_resource (css_provider, "/org/tabos/tvguide/style.css");
  gtk_style_context_add_provider_for_screen (gdk_screen_get_default (),
                                             GTK_STYLE_PROVIDER (css_provider),
                                             GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  g_object_unref (css_provider);

  g_action_map_add_action_entries (G_ACTION_MAP (app),
                                   app_entries, G_N_ELEMENTS (app_entries),
                                   app);

  soup_session_sync = soup_session_new ();
}

static void
show_window (GtkApplication *app)
{
  window = tvguide_window_new (app);

  if (tvguide_window_load_database (window))
    gtk_widget_show (GTK_WIDGET (window));
}

int
main (int    argc,
      char **argv)
{
  int status;

  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  /* Set application name */
  g_set_prgname(PACKAGE_NAME);
  g_set_application_name(PACKAGE_NAME);

  app = gtk_application_new ("org.tabos.tvguide", G_APPLICATION_FLAGS_NONE);

  gtk_window_set_default_icon_name ("org.tabos.tvguide");

  g_signal_connect (app, "startup", G_CALLBACK (startup), NULL);
  g_signal_connect (app, "activate", G_CALLBACK (show_window), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
