/**
 * TV Guide
 * Copyright (c) 2017-2018 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>

#include <gtk/gtk.h>
#include <glib/gi18n.h>

#define HANDY_USE_UNSTABLE_API
#include <libhandy-0.0/handy.h>


#include "tvguide-assistant.h"
#include "tvguide-config.h"
#include "tvguide-utils.h"
#include "tvguide-window.h"
#include "tvguide-xml.h"

/** Assistant page structure, holding name and pre/post functions */
typedef struct assistant_page
{
  /*< private >*/
  gchar *name;
  void (*pre)(TvGuideAssistant * self);
  gboolean (*post)(TvGuideAssistant *self);
} AssistantPage;

struct _TvGuideAssistant
{
  GtkAssistant parent_instance;

  TvGuideWindow *window;
  GtkWidget *country_listbox;
  GtkWidget *channel_listbox;
  GtkWidget *channel_all_button;
  GtkWidget *channel_none_button;

  gint current_page;
  gint max_page;
  gchar *country;
};

G_DEFINE_TYPE (TvGuideAssistant, tvguide_assistant, GTK_TYPE_ASSISTANT)

enum
{
  PROP_0,
  PROP_WINDOW,
  LAST_PROP,
};

static GParamSpec *obj_properties[LAST_PROP];

static void
widget_destroy (GtkWidget *widget,
                gpointer   user_data)
{
  gtk_widget_destroy (widget);
}

static gboolean
assistant_load_countries (gpointer user_data)
{
  TvGuideAssistant *self = user_data;
  gchar *data;
  gsize size;
  xmlnode *node;
  xmlnode *child;

  data = tvguide_read_url ("http://xmltv.se/countries.xml", &size);
  node = xmlnode_from_str (data, size);
  g_return_val_if_fail (node != NULL, G_SOURCE_REMOVE);

  gtk_container_foreach (GTK_CONTAINER (self->country_listbox), widget_destroy, NULL);

  child = node;
  for (child = xmlnode_get_child (child, "country"); child != NULL; child = xmlnode_get_next_twin (child)) {
    GtkWidget *country;
    gchar *tmp;

    tmp = g_strdup_printf ("%s", xmlnode_get_data (child));

    country = gtk_label_new ("");
    gtk_label_set_justify (GTK_LABEL (country), GTK_JUSTIFY_CENTER);
    gtk_label_set_markup (GTK_LABEL (country), tmp);
    gtk_widget_set_margin (country, 12, 6, 12, 6);
    g_free (tmp);
    gtk_widget_show (country);

    gtk_list_box_insert (GTK_LIST_BOX (self->country_listbox), country, -1);
    GtkWidget *separator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
    gtk_widget_show (separator);
    gtk_list_box_insert (GTK_LIST_BOX (self->country_listbox), separator, -1);
  }

  return G_SOURCE_REMOVE;
}

static void
country_listbox_row_activated_cb (GtkListBox       *box,
                                            GtkListBoxRow    *row,
                                            TvGuideAssistant *self)
{
  GtkWidget *page;
  if (row != NULL) {
    GtkWidget *child = gtk_container_get_children (GTK_CONTAINER (row))->data;

    self->country = g_strdup (gtk_label_get_text (GTK_LABEL (child)));

    page = gtk_assistant_get_nth_page (GTK_ASSISTANT (self), gtk_assistant_get_current_page (GTK_ASSISTANT (self)));
    gtk_assistant_set_page_complete (GTK_ASSISTANT (self), page, TRUE);
  }
}

static gboolean
assistant_load_channels (gpointer user_data)
{
  TvGuideAssistant *self = TVGUIDE_ASSISTANT (user_data);
  gchar *data;
  gsize size;
  xmlnode *node;
  xmlnode *child;
  gchar *url;

  url = g_strdup_printf ("http://xmltv.xmltv.se/channels-%s.xml.gz", self->country);
  data = tvguide_read_url (url, &size);
  node = xmlnode_from_str (data, size);
  g_return_val_if_fail (node != NULL, G_SOURCE_REMOVE);

  /* For all children of list box, call router_listbox_destroy() */
  gtk_container_foreach (GTK_CONTAINER (self->channel_listbox), widget_destroy, NULL);

  child = node;
  for (child = xmlnode_get_child (child, "channel"); child != NULL; child = xmlnode_get_next_twin (child)) {
    GtkWidget *channel;
    GtkWidget *box;
    gchar *tmp;
    xmlnode *chan;
    gchar *id;

    chan = xmlnode_get_child (child, "display-name");

    id = g_strdup (xmlnode_get_attrib (child, "id"));
    tmp = g_strdup_printf ("%s", xmlnode_get_data (chan));
    if (tmp == NULL || strlen (tmp) <= 0) {
      tmp = g_strdup (id);
    }

    channel = gtk_label_new (tmp);
    gtk_label_set_xalign (GTK_LABEL (channel), 0);
    gtk_label_set_line_wrap (GTK_LABEL (channel), TRUE);

    g_object_set_data (G_OBJECT (channel), "id", id);

    box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_widget_set_margin (box, 12, 6, 12, 6)
    gtk_widget_show (box);

    g_free (tmp);
    gtk_widget_show (channel);

#ifdef GTK4
    gtk_box_pack_start (GTK_BOX (box), channel);
#else
    gtk_box_pack_start (GTK_BOX (box), channel, TRUE, TRUE, 0);
#endif
    GtkWidget *channel_switch = gtk_switch_new ();
    gtk_switch_set_active (GTK_SWITCH (channel_switch), TRUE);
    gtk_widget_show (channel_switch);
#ifdef GTK4
    gtk_box_pack_end (GTK_BOX (box), channel_switch);
#else
    gtk_box_pack_end (GTK_BOX (box), channel_switch, FALSE, FALSE, 0);
#endif

    gtk_list_box_insert (GTK_LIST_BOX (self->channel_listbox), box, -1);

    GtkWidget *separator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
    gtk_widget_show (separator);
    gtk_list_box_insert (GTK_LIST_BOX (self->channel_listbox), separator, -1);
  }

  return G_SOURCE_REMOVE;
}

static void
channel_all_button_clicked_cb (GtkButton *widget,
                               gpointer   user_data)
{
  TvGuideAssistant *self = TVGUIDE_ASSISTANT (user_data);
  GList *list = NULL;

  for (list = gtk_container_get_children (GTK_CONTAINER (self->channel_listbox)); list != NULL; list = list->next) {
    GtkWidget *row = list->data;
    GList *row_children = gtk_container_get_children (GTK_CONTAINER (row));
    GtkWidget *box = row_children->data;

    if (GTK_IS_SEPARATOR (box)) {
      continue;
    }

    GList *box_children = gtk_container_get_children (GTK_CONTAINER (box));
    box_children = box_children->next;
    GtkWidget *on_switch = box_children->data;

    gtk_switch_set_active (GTK_SWITCH (on_switch), TRUE);
  }
}

static void
channel_none_button_clicked_cb (GtkButton *widget,
                                gpointer   user_data)
{
  TvGuideAssistant *self = TVGUIDE_ASSISTANT (user_data);
  GList *list = NULL;

  for (list = gtk_container_get_children (GTK_CONTAINER (self->channel_listbox)); list != NULL; list = list->next) {
    GtkWidget *row = list->data;
    GList *row_children = gtk_container_get_children (GTK_CONTAINER (row));
    GtkWidget *box = row_children->data;

    if (GTK_IS_SEPARATOR (box)) {
      continue;
    }

    GList *box_children = gtk_container_get_children (GTK_CONTAINER (box));
    box_children = box_children->next;
    GtkWidget *on_switch = box_children->data;

    gtk_switch_set_active (GTK_SWITCH (on_switch), FALSE);
  }
}

static void
tvguide_assistant_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  TvGuideAssistant *self = TVGUIDE_ASSISTANT (object);

  switch (prop_id) {
    case PROP_WINDOW: {
      self->window = g_value_get_object (value);
    }
    break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
tvguide_assistant_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  TvGuideAssistant *self = TVGUIDE_ASSISTANT (object);

  switch (prop_id) {
    case PROP_WINDOW: {
      g_value_set_object (value, self->window);
    }
    break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
on_prepare_cb (TvGuideAssistant *self,
               GtkWidget        *page,
               gpointer          user_data)

{
  gint current_page = gtk_assistant_get_current_page (GTK_ASSISTANT (self));

  switch (current_page) {
  case 0:
    gtk_assistant_set_page_complete (GTK_ASSISTANT (self), page, TRUE);
    break;
  case 1:
    /* Start loading of countries */
    g_idle_add (assistant_load_countries, self);
    gtk_assistant_set_page_complete (GTK_ASSISTANT (self), page, FALSE);
    break;
  case 2:
    /* Start loading of countries */
    g_idle_add (assistant_load_channels, self);
    gtk_assistant_set_page_complete (GTK_ASSISTANT (self), page, TRUE);
    break;
  case 3: {
    GList *list;
    GKeyFile *file = g_key_file_new ();

    g_key_file_set_string (file, "General", "Country", self->country);

    for (list = gtk_container_get_children (GTK_CONTAINER (self->channel_listbox)); list != NULL; list = list->next) {
      GtkWidget *row = list->data;
      GList *row_children = gtk_container_get_children (GTK_CONTAINER (row));
      GtkWidget *box = row_children->data;

      if (GTK_IS_SEPARATOR (box)) {
        continue;
      }

      GList *box_children = gtk_container_get_children (GTK_CONTAINER (box));
      GtkWidget *label = box_children->data;
      box_children = box_children->next;
      GtkWidget *on_switch = box_children->data;

      gchar *id = g_object_get_data (G_OBJECT (label), "id");
      g_key_file_set_boolean (file, id, "enabled", gtk_switch_get_active (GTK_SWITCH (on_switch)));
      g_key_file_set_string (file, id, "name", gtk_label_get_text (GTK_LABEL (label)));
      gchar *icon = g_strdup_printf ("https://chanlogos.xmltv.se//%s.png", id);
      g_key_file_set_string (file, id, "icon", icon);
    }

    tvguide_save_key_file (file);

    gtk_assistant_set_page_complete (GTK_ASSISTANT (self), page, TRUE);
    break;
  }
  default:
    break;
  }
}

static void
on_cancel_cb (TvGuideAssistant *self,
              gpointer          user_data)
{
  gtk_widget_destroy (GTK_WIDGET (self));

  tvguide_window_show_main (self->window);
}

static void
on_close_cb (TvGuideAssistant *self,
             gpointer          user_data)
{
  gtk_widget_destroy (GTK_WIDGET (self));

  tvguide_window_load_database (self->window);
  tvguide_window_show_main (self->window);
}

static void
tvguide_assistant_constructed (GObject *object)
{
  G_OBJECT_CLASS (tvguide_assistant_parent_class)->constructed (object);
}

static void
tvguide_assistant_class_init (TvGuideAssistantClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = tvguide_assistant_set_property;
  object_class->get_property = tvguide_assistant_get_property;
  object_class->constructed = tvguide_assistant_constructed;

  obj_properties[PROP_WINDOW] =
    g_param_spec_object ("window",
                         "TV Guide Window",
                         "TV Guide Window",
                         G_TYPE_OBJECT,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties (object_class, LAST_PROP, obj_properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/tabos/tvguide/ui/tvguide-assistant.ui");

  gtk_widget_class_bind_template_child (widget_class, TvGuideAssistant, country_listbox);
  gtk_widget_class_bind_template_child (widget_class, TvGuideAssistant, channel_listbox);
  gtk_widget_class_bind_template_child (widget_class, TvGuideAssistant, channel_all_button);
  gtk_widget_class_bind_template_child (widget_class, TvGuideAssistant, channel_none_button);

  gtk_widget_class_bind_template_callback (widget_class, channel_all_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, channel_none_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, country_listbox_row_activated_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_prepare_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_cancel_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_close_cb);
}

static void
tvguide_assistant_init (TvGuideAssistant *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

TvGuideAssistant *
tvguide_assistant_new (TvGuideWindow *window)
{
  return g_object_new (TVGUIDE_TYPE_ASSISTANT, "window", window, NULL);
}
